// variable
var windowWidth =  $(window).width();

$(window).scroll(function(){
	if($(window).scrollTop() > 0) {
		// header
		$(".navbar-align").addClass("header-align");
		$(".navbar-brand img").attr("src","assets/images/banner/logo1.png");
		if(windowWidth > 991){
			$(".navright-btn li").addClass("active-border");
			$(".navright-btn li a").addClass("active-color");
			$(".navbar-right li a").css("color","#636161");
		}
		if(windowWidth <= 991){
			$(".navbar-align .navbar-toggle .icon-bar").css("background-color","#009d75");
			$(".navbar-collapse").css("margin-top","0px")
		}
	}else if($(window).scrollTop() == 0){
		// header
		$(".navbar-align").removeClass("header-align");
		$(".navbar-brand img").attr("src","assets/images/banner/logo.png");
		if(windowWidth > 991){
			$(".navright-btn li").removeClass("active-border");
			$(".navright-btn li a").removeClass("active-color");
			$(".navbar-right li a").css("color","#fff");
		}
		if(windowWidth <= 991){
			$(".navbar-align .navbar-toggle .icon-bar").css("background-color","#fff");
			$(".navbar-collapse").css("margin-top","10px")
		}
	}
});

// benefits jquery
var slides = document.querySelectorAll('.mob-benefit .benifts-desc');
var slides_hex = document.querySelectorAll('.hex');

var currentSlide = 0;

function nextSlide(){
	goToSlide(currentSlide+1);
}

function previousSlide(){
	goToSlide(currentSlide-1);
}

function goToSlide(n){
	// content
	current_odd = slides[currentSlide];
	$(current_odd).addClass("hide")

	currentSlide = (n+slides.length)%slides.length;
	slides[currentSlide].className = 'benifts-desc';

	// polygon
	$(".hex").removeClass("active")
	currentSlide = (n+slides_hex.length)%slides_hex.length;
	$(slides_hex[currentSlide]).addClass("active")
}

var next = document.getElementById('next');
var previous = document.getElementById('previous');

next.onclick = function() {
	nextSlide();
};
previous.onclick = function() {
	previousSlide();
};


// polygon click to change text
$('.hex').click(function(e){
	e.preventDefault();
	var menu = $(this).attr('data-menu');
	$(".benifts-desc").addClass("hide");
	$(".hex").removeClass("active");
	$('.'+ menu + '-section').removeClass('hide');
	$(this).addClass("active");
});

// polygon shape change
function polygonshape(){
	if(windowWidth > 991 && windowWidth < 1260){
		$(".benefits-polygon svg").find("polygon").attr("points","89.2,78.3 45.3,104.3 1.3,78.2 1.2,26 45.1,0 89.1,26.1")
		$(".benefits-polygon svg").attr({ width: '100px', height: '130px'});
		$(".benefits-polygon svg pattern").attr({ x: '-30', y: '-30'});
	}
}
polygonshape();

// animation
$(window).on('load', function(){
	if(windowWidth > 991){
		$(".banner-notes h5").addClass("slidedown");
		$(".banner-notes ul").addClass("slideup");
	}
});

$("#how-it-works").each(function (index) {
	var $this     = $(this),
	offsetTop = $this.offset().top;
	if (scrolled + win_height_padded < offsetTop) {
		$(".work-process").addClass("slideup");
	}
});
